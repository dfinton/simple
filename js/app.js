var hello = function(data) {
    $('.hello').html(data);
};

var world = function(data) {
    $('.world').html(data);
};

var helloPromise = $.ajax({
    url : 'hello.html'
});

var worldPromise = $.ajax({
    url : 'world.html'
});

$(document).ready(function() {
    helloPromise.done(hello);
    worldPromise.done(world);
});
